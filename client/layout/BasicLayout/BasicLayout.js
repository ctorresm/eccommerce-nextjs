import { Container } from "@material-ui/core";
import Header from "../../components/Header";
import classNames from "classnames";
import React from "react";

const BasicLayout = (props) => {
  const { children, className } = props;

  const body = (
    <div className="layout__main">
      <Header />
      <Container
        maxWidth="lg"
        className={classNames("basic-layout", {
          [className]: className,
          "shadow-2xl": true,
        })}
      >
        {children}
      </Container>
    </div>
  );

  return body;
};

export default BasicLayout;
