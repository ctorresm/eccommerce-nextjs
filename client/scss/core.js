import { createMuiTheme } from "@material-ui/core";

const coreThemeObj = {
  typography: {
    fontFamily: ["Arial", "Roboto"].join(","),
  },
  palette: {
    primary: {
      main: "#e50914",
    },
    secondary: {
      main: "#0070f3",
    },
  },
};

export const darkTheme = createMuiTheme({
  ...coreThemeObj,
  palette: {
    ...coreThemeObj.palette,
    type: "dark",
  },
});

export const lightTheme = createMuiTheme({
  ...coreThemeObj,
  palette: {
    ...coreThemeObj.palette,
    type: "light",
  },
});
