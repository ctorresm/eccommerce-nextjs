import { Button, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { updateNameApi } from "../../../api/user.service";
import { toast } from "react-toastify";

const schema = yup.object().shape({
  name: yup.string().required(true),
  lastName: yup.string().required(true),
});

export default function ChangeNameForm(props) {
  const { user, logout, setReloadUser } = props;
  const [loading, setLoading] = useState(false);

  const { register, handleSubmit, errors, formState } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data) => {
    setLoading(true);
    const response = await updateNameApi(user.id, data, logout);
    if (!response) {
      toast.error("Eror al actualizar su información");
    } else {
      setReloadUser(true);
      toast.success("Nombre y apellido actualizados");
    }
    setLoading(false);
  };

  return (
    <div>
      <h1 className="text-xl">Cambia tu nombre y apellido</h1>
      <form
        className="grid md:grid-cols-3 sm: grid-cols-1 gap-4 mt-4"
        onSubmit={handleSubmit(onSubmit)}
        noValidate
      >
        <div>
          <TextField
            variant="filled"
            required
            error={errors["name"] ? true : false}
            className="mb-3"
            name="name"
            label="Nombre"
            helperText={errors["name"] ? "Campo Obligatorio" : ""}
            inputRef={register}
            fullWidth
          />
        </div>
        <TextField
          variant="filled"
          required
          error={errors["lastName"] ? true : false}
          name="lastName"
          className="mb-3"
          label="Apellidos"
          helperText="Campo obligatorio"
          helperText={errors["lastName"] ? "Campo Obligatorio" : ""}
          inputRef={register}
          fullWidth
        />
        <TextField
          variant="filled"
          required
          name="username"
          className="mb-3"
          label="Nombre de usuario"
          value={user.username}
          fullWidth
        />
        <span></span>
        <span></span>
        <Button type="submit" variant="contained" color="primary">
          Actualizar
        </Button>
      </form>
    </div>
  );
}
