import React, { useState } from "react";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

const Auth = (props) => {
  const { handleClose, setTitle } = props;

  const [login, setLogin] = useState(true);

  const showLoginForm = () => {
    setLogin(true);
    setTitle("Iniciar sesión");
  };
  const showRegisterForm = () => {
    setLogin(false);
    setTitle("Regístrate");
  };

  return login ? (
    <LoginForm showRegisterForm={showRegisterForm} handleClose={handleClose} />
  ) : (
    <RegisterForm showLoginForm={showLoginForm} />
  );
};

export default Auth;
