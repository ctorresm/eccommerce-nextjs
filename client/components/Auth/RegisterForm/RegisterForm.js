import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Button, CircularProgress, TextField } from "@material-ui/core";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { registerUser } from "../../../api/user.service";
import { toast } from "react-toastify";

const schema = yup.object().shape({
  name: yup.string().required(true),
  lastName: yup.string().required(true),
  username: yup.string().required(true),
  email: yup.string().email().required(true),
  password: yup.string().required(true),
});

const RegisterForm = (props) => {
  const [loading, setLoading] = useState(false);

  const { register, handleSubmit, errors, formState } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema),
  });
  const { showLoginForm } = props;

  const onSubmit = async (data) => {
    setLoading(true);
    const response = await registerUser(data);
    if (response?.jwt) {
      showLoginForm;
      toast.success("Registrado correctamente");
    } else {
      toast.error("Error al registrar el usuario 🦄");
    }
    setLoading(false);
  };

  return (
    <div>
      <form
        className="px-8 pb-8 pt-4"
        onSubmit={handleSubmit(onSubmit)}
        noValidate
      >
        <TextField
          required
          error={errors["name"] ? true : false}
          className="mb-4"
          name="name"
          label="Nombre"
          fullWidth
          inputRef={register}
        />

        <TextField
          required
          name="lastName"
          error={errors["lastName"] ? true : false}
          className="mb-4"
          label="Apellidos"
          inputRef={register}
          fullWidth
        />

        <TextField
          required
          name="username"
          className="mb-4"
          error={errors["username"] ? true : false}
          label="Nombre de usuario"
          inputRef={register}
          fullWidth
        />
        <TextField
          required
          className="mb-4"
          name="email"
          error={errors["email"] ? true : false}
          label="Correo electrónico"
          inputRef={register}
          fullWidth
        />

        <TextField
          required
          type="password"
          error={errors["password"] ? true : false}
          className="mb-4"
          name="password"
          label="Contraseña"
          inputRef={register}
          fullWidth
        />
        <div className="flex-v-center mt-4">
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disabled={loading}
          >
            {loading && (
              <CircularProgress
                variant="indeterminate"
                color="secondary"
                size="20px"
              />
            )}
            {!loading && "Registrar"}
          </Button>
        </div>
      </form>
      <hr></hr>
      <div className="text-center">
        <Button color="primary" onClick={showLoginForm}>
          Ya tengo una cuenta
        </Button>
      </div>
    </div>
  );
};

export default RegisterForm;
