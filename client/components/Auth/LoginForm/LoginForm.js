import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Button, CircularProgress, TextField } from "@material-ui/core";
import { yupResolver } from "@hookform/resolvers/yup";
import { loginApi } from "../../../api/user.service";
import { toast } from "react-toastify";
import useAuth from "../../../hooks/useAuth";
import * as yup from "yup";

const schema = yup.object().shape({
  identifier: yup.string().email().required(true),
  password: yup.string().required(true),
});

const LoginForm = (props) => {
  const [loading, setLoading] = useState(false);
  const { showRegisterForm, handleClose } = props;
  const { login } = useAuth();
  const { register, handleSubmit, errors } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data) => {
    setLoading(true);
    const response = await loginApi(data);
    setLoading(false);
    if (response?.jwt) {
      login(response.jwt);
      toast.success("Bienvenido");
      handleClose(true);
    } else {
      toast.error("Usuario y/o password incorrecto");
    }
    setLoading(false);
  };

  return (
    <div>
      <div className="flex-v-center mt-8">
        <img src="logo.png" width="150px"></img>
      </div>

      <form
        className="px-8 pb-8 pt-8"
        onSubmit={handleSubmit(onSubmit)}
        noValidate
      >
        <TextField
          required
          className="mb-4"
          name="identifier"
          error={errors["identifier"] ? true : false}
          label="Correo electrónico"
          inputRef={register}
          fullWidth
        />

        <TextField
          required
          type="password"
          error={errors["password"] ? true : false}
          className="mb-4"
          name="password"
          label="Contraseña"
          inputRef={register}
          fullWidth
        />
        <div className="flex-v-center mt-4">
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disabled={loading}
          >
            {loading && (
              <CircularProgress
                variant="indeterminate"
                color="secondary"
                size="20px"
              />
            )}
            {!loading && "Iniciar sesión"}
          </Button>
        </div>
        <div className="text-center mt-2">
          <Button className="text-xs">Olvidé mi contraseña</Button>
        </div>
      </form>
      <hr></hr>
      <Button
        variant="text"
        className="flex w-full mt-2"
        onClick={showRegisterForm}
      >
        Aún no tengo una cuenta
      </Button>
    </div>
  );
};

export default LoginForm;
