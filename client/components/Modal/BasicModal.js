import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
} from "@material-ui/core";
import React from "react";
import { MdClose } from "react-icons/md";

import styled from "styled-components";

const Title = styled.div`
  background: var(--background-grey-dark);
  padding: 8px 24px;
  width: 100%;
  font-size: 1.375rem;
`;

const BasicModal = (props) => {
  const { show, onClose, title, children, size, ...rest } = props;
  const [fullWidth, setFullWidth] = React.useState(true);

  const handleClose = (value = false) => {
    onClose(value);
  };

  return (
    <React.Fragment>
      <Dialog
        fullWidth={fullWidth}
        open={show}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
        {...rest}
      >
        <Title className="w-full flex-v-center">
          <span className="text-yellow-50">{title}</span>
          <span className="flex-spacer"></span>
          <IconButton
            onClick={handleClose}
            className="text-white"
            aria-label="add an alarm"
          >
            <MdClose />
          </IconButton>
        </Title>
        <DialogContent>{children}</DialogContent>
        {/* <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions> */}
      </Dialog>
    </React.Fragment>
  );
};

export default BasicModal;
