import { Button, ButtonGroup, Grid, IconButton } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Link from "next/link";
import {
  MdExitToApp,
  MdFavorite,
  MdMoveToInbox,
  MdPerson,
  MdShoppingCart,
} from "react-icons/md";
import styled from "styled-components";
import BasicModal from "../../Modal/BasicModal";
import Auth from "../../Auth";
import useAuth from "../../../hooks/useAuth";
import { getMeApi } from "../../../api/user.service";

const MenuButton = styled(Button)`
  color: #fff;
  &:hover {
    background-color: var(--background-dark);
  }
`;

export default function MenuWeb() {
  const { auth, logout } = useAuth();
  const [user, setUser] = useState(undefined);
  const [title, setTitle] = useState("Iniciar sesión");
  const [showModal, setShowModal] = useState(false);

  const handleClose = () => setShowModal(false);
  useEffect(() => {
    (async () => {
      const response = await getMeApi(logout);
      setUser(response);
    })();
  }, [auth]);

  return (
    <div>
      <Grid container className="menuWeb">
        <Grid item md={1}></Grid>
        <Grid item md={5}>
          <MenuPlatforms />
        </Grid>
        <Grid item container md={5} alignItems="center" justify="flex-end">
          {user !== undefined && (
            <MenuOptions
              auth={auth}
              user={user}
              logout={logout}
              setShowModal={setShowModal}
            />
          )}
        </Grid>
        <Grid item md={1}></Grid>
      </Grid>

      <BasicModal
        show={showModal}
        title={title}
        maxWidth={"xs"}
        onClose={handleClose}
        disableBackdropClick={true}
      >
        <Auth handleClose={handleClose} setTitle={setTitle} />
      </BasicModal>
    </div>
  );
}

function MenuPlatforms() {
  return (
    <div>
      <Link href="/play-station">
        <MenuButton>PlayStation</MenuButton>
      </Link>
      <Link href="/xbox">
        <MenuButton>Xbox</MenuButton>
      </Link>
      <Link href="/switch">
        <MenuButton>Switch</MenuButton>
      </Link>
    </div>
  );
}

function MenuOptions(props) {
  const { auth, user, logout, setShowModal } = props;

  return (
    <div>
      {!auth && (
        <MenuButton onClick={() => setShowModal(true)}>
          <MdPerson className="mr-2 text-xl text-yellow-700" />
          Mi Cuenta
        </MenuButton>
      )}
      {auth && (
        <>
          <Link href="/pedidos">
            <MenuButton className="mx-1 px-2 ">
              <MdMoveToInbox className="text-xl text-yellow-50 mr-2" />
              Pedidos
            </MenuButton>
          </Link>

          <Link href="/wishlist">
            <MenuButton className="mx-1 px-2">
              <MdFavorite className="text-xl text-yellow-50 mr-2" />
              Favoritos
            </MenuButton>
          </Link>

          <Link href="/account">
            <MenuButton className="mx-1 px-2">
              <MdPerson className=" text-xl text-yellow-50 mr-2" />
              {user?.name} {user?.lastName}
            </MenuButton>
          </Link>
          <MenuButton onClick={logout}>
            <MdShoppingCart className="text-xl text-yellow-700" />
          </MenuButton>

          <MenuButton onClick={logout}>
            <MdExitToApp className="text-xl text-red-700" />
          </MenuButton>
        </>
      )}
    </div>
  );
}
