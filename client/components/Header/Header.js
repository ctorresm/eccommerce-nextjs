import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MenuWeb from "./Menu";
import styled from "styled-components";
import Link from "next/link";

const Input = styled.input`
  width: 500px;
  color: black;
`;

export default function Header() {
  return (
    <div className="header">
      <div>
        <AppBar position="static">
          <Toolbar className="toolbar flex-v-center">
            <Link href="/">
              <img src="logo.png" width="200px"></img>
            </Link>
            <span className="flex-spacer"></span>
            <Input />
          </Toolbar>
          <MenuWeb />
        </AppBar>
      </div>
    </div>
  );
}
