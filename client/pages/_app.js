import "../scss/global.scss";
import "tailwindcss/tailwind.css";
import "react-toastify/dist/ReactToastify.css";
import React, { useMemo, useState, useEffect } from "react";
import AuthContext from "../context/AuthContext";
import { ToastContainer } from "react-toastify";
import { StylesProvider } from "@material-ui/core/styles";
import { getToken, setToken, removeToken } from "../api/token.service";
import jwtDecode from "jwt-decode";

export default function MyApp({ Component, pageProps }) {
  React.useEffect(() => {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  const [auth, setAuth] = useState(undefined);
  const [reloadUser, setReloadUser] = useState(false);


  useEffect(() => {
    setReloadUser(false);
    const token = getToken();
    if (token) {
      setAuth({
        token,
        idUser: jwtDecode(token).id,
      });
    } else {
      setAuth(null);
    }
  }, [reloadUser]);

  const login = (token) => {
    setToken(token);
    setAuth({
      token,
      idUser: jwtDecode(token).id,
    });
  };

  const logout = () => {
    if (auth) {
      removeToken();
      setAuth(null);
    }
  };

  const authData = useMemo(
    () => ({
      auth,
      login,
      logout,
      setReloadUser,
    }),
    [auth]
  );

  if (auth === undefined) return null;

  return (
    <AuthContext.Provider value={authData}>
      <StylesProvider injectFirst>
        <Component {...pageProps} />
        <ToastContainer
          position="top-right"
          autoClose={4000}
          hideProgressBar
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnFocusLoss={false}
          draggable
          pauseOnHover
        />
      </StylesProvider>
    </AuthContext.Provider>
  );
}
