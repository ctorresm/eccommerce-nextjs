import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import BasicLayout from "../layout/BasicLayout/BasicLayout";
import useAuth from "../hooks/useAuth";
import { getMeApi } from "../api/user.service";
import { route } from "next/dist/next-server/server/router";
import ChangeNameForm from "../components/Account/ChangeNameForm";
import { Card, LinearProgress } from "@material-ui/core";
import Head from "next/head";

export default function account() {
  const router = useRouter();
  const { auth, logout, setReloadUser } = useAuth();
  const [user, setUser] = useState(null);

  useEffect(() => {
    (async () => {
      const response = await getMeApi(logout);
      setUser(response || null);
    })();
  }, [auth]);

  if (user === undefined) return null;

  if (!auth && !user) {
    router.replace("/");
    return null;
  }

  const Configuration = () => {
    return (
      <div className="py-4">
        <Head>
          <title>Mi Cuenta</title>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
            key="viewport"
          />
        </Head>
        <div className="bg-gray-900 text-white p-4">Configuración</div>
        <Card className="p-6">
          {!user && <LinearProgress />}
          {user && (
            <ChangeNameForm
              user={user}
              logout={logout}
              setReloadUser={setReloadUser}
            />
          )}
        </Card>

        <hr></hr>

        <div className="bg-gray-900 text-white p-4 mt-8">Direcciones</div>
        <Card className="p-6">
          <div>Direcciones</div>
        </Card>
      </div>
    );
  };

  return (
    <BasicLayout className="account">
      <Configuration />
    </BasicLayout>
  );
}
